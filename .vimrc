call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree'
Plug 'kien/ctrlp.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'vim-ruby/vim-ruby'
Plug 'tmhedberg/SimpylFold'
Plug 'vim-scripts/indentpython.vim'
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8'
Plug 'tpope/vim-fugitive'
Plug 'ycm-core/YouCompleteMe'
Plug 'powerline/powerline', {'rtp': 'powerline/bindings/vim/'}
call plug#end()

set termguicolors
set number
set foldmethod=indent
set foldlevel=0
set encoding=utf-8
set nobackup nowritebackup
set laststatus=2
set rtp+=~/.vim/plugged/powerline/powerline/bindings/vim
set guifont=Literation\ Mono\ Powerline:h15

syntax on
colorscheme nord

let mapleader = ','
let python_highlight_all=1
let NERDTreeIgnore=['\.pyc$', '\~$']
let g:ycm_autoclose_preview_window_after_completion=1
let g:SimpylFold_docstring_preview=1
let g:Powerline_symbols = 'fancy'

map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"easier window navigation
 
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l
 
"Resize vsplit

nmap 25 :vertical resize 40<cr>
nmap 50 <c-w>=
nmap 75 :vertical resize 120<cr>
 
nmap <C-b> :NERDTreeToggle<cr>

nnoremap <space> za

au BufNewFile, BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

au BufRead, BufNewFile *.py,*.pyw match BadWhitespace /\s\+$/

autocmd FileType python map <buffer> <leader>8 :call flake8#Flake8()<CR>

"python with virtualenv support

py3 << EOF
import os

if 'VIRTUAL_ENV' in os.environ:
  activate_this = os.path.join(os.environ.get('VIRTUAL_ENV'), '/bin/activate')
  if os.path.isfile(activate_this):
      exec(open(activate_this))
EOF
